import { useParams } from 'react-router-dom'
import { useGetCustomerQuery } from '../app/fridgeSlice'
import { motion } from 'framer-motion'

const Customer = () => {
    const { item_id } = useParams()
    const { data, isLoading, isError } = useGetCustomerQuery(item_id)

    if (isLoading)
        return <div className="text-center text-blue-500">Loading...</div>
    if (isError || !data)
        return (
            <div className="text-center text-red-500">
                Error loading customer details.
            </div>
        )

    return (
        <div className="bg-blue-50 min-h-screen flex items-center justify-center">
            <motion.div
                initial={{ opacity: 0, y: -20 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.5 }}
                className="bg-white shadow-lg rounded-lg p-8 border border-blue-200 max-w-lg w-full"
            >
                <div className="text-xl font-bold mb-6 text-blue-800">
                    Beverage Details
                </div>
                <div className="space-y-4">
                    <h2 className="text-lg font-semibold text-gray-900">
                        {data.name}
                    </h2>
                    <p className="text-gray-700">
                        phone_number:{' '}
                        <span className="font-semibold">${data.phone_number}</span>
                    </p>
                    <p className="text-gray-700">
                        email:{' '}
                        <span className="font-semibold">{data.email}</span>
                    </p>
                    <p className="text-gray-700">
                        vehicle_year:{' '}
                        <span className="font-semibold">{data.vehicle_year}</span>
                    </p>
                    <p className="text-gray-700">
                        vehicle_make:{' '}
                        <span className="font-semibold">{data.vehicle_make}</span>
                    </p>
                    <p className="text-gray-700">
                        vehicle_model:{' '}
                        <span className="font-semibold">{data.vehicle_model}</span>
                    </p>
                </div>
            </motion.div>
        </div>
    )
}

export default Customer
