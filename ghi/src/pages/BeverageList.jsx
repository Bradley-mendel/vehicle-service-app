import {
    useGetAllCustomersQuery,
    useDeleteCustomerMutation,
} from '../app/fridgeSlice'
import { Link } from 'react-router-dom'
import { motion } from 'framer-motion'
import { useState } from 'react'

function CustomerList() {
    const { data, isLoading } = useGetAllCustomersQuery()
    const [deleteCustomer] = useDeleteCustomerMutation()
    const handleDelete = async (item_id) => {
        try {
            await deleteCustomer(item_id)
        } catch (error) {
            console.error('Error deleting item:', error)
        }
    }
    const submitToRedux = (e, item_id) => {
        e.preventDefault()
        changeName(item_id)
    }
    const [lightOn, setLightOn] = useState(true)
    const toggleLight = () => setLightOn(!lightOn)
    if (isLoading)
        return <div className="text-center text-blue-500">Loading...</div>
    return (
        <div className="p-6 fridge-bg min-h-screen">
            <h1 className="text-2xl font-bold mb-6 text-blue-800">
                Customer is in the system
            </h1>
            <div
                className={`p-6 ${lightOn ? 'bg-blue-400' : 'bg-gray-800'
                    } min-h-screen transition duration-500`}
            >
                <button onClick={toggleLight} className="btn btn-sm">
                    {lightOn ? 'Turn Light Off' : 'Turn Light On'}
                </button>
                <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
                    {data.map((customer, index) => (
                        <motion.div
                            key={customer.id}
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1 }}
                            transition={{ delay: index * 0.1 }}
                            className={`p-4 bg-blue-800 rounded-lg shadow-lg ${index < data.length - 1 ? 'shelf' : ''
                                }`}
                        >
                            <h3 className="font-bold">{customer.name}</h3>
                            <p>phone_number: {customer.phone_number}</p>
                            <p>Email: {customer.email}</p>
                            <p>Vehicle_year: {customer.vehicle_year}</p>
                            <p>Vehicle_make: {customer.vehicle_make}</p>
                            <p>Vehicle_model: {customer.vehicle_model}</p>
                            <form onSubmit={(e) => submitToRedux(e, item_id)}>
                            <div className="flex justify-between mt-4">
                                <Link
                                    to={`/beverages/${customer.id}`}
                                    className="btn btn-sm btn-info"
                                >
                                    Details
                                </Link>
                                <button
                                    onClick={() => handleDelete(customer.id)}
                                    className="btn btn-sm btn-error"
                                >
                                    Delete
                                </button>
                                <Link
                                    to={`/customers/${customer.id}/update`}
                                    className="btn btn-sm btn-warning"
                                >
                                    Update
                                </Link>
                            </div>
                        </form>
                        </motion.div>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default CustomerList
