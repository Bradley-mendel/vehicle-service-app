import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetCustomerQuery,
    useUpdateCustomerMutation,
} from '../app/fridgeSlice'
// import { Phone_n } from 'lucide-react'

function UpdateCustomer() {
    const { item_id } = useParams()
    const navigate = useNavigate()
    const { data: customer, error } = useGetCustomerQuery(item_id)
    const [updateCustomer] = useUpdateCustomerMutation()
    const [isLoading, setIsLoading] = useState(false)

    const [formData, setFormData] = useState({
        name: '',
        phone_number: '',
        email: '',
        vehicle_year: '',
        vehicle_make: '',
        vehicle_model: '',
    })
    useEffect(() => {
        if (customer) {
            setFormData({
                name: customer.name,
                phone_number: customer.phone_number,
                email: customer.email,
                vehicle_year: customer.vehicle_year,
                vehicle_make: customer.vehicle_make,
                vehicle_model: customer.vehicle_model,
            })
        }
    }, [customer])

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        setIsLoading(true)
        try {
            await updateCustomer({ item_id, updatedData: formData }).unwrap()
            navigate('/customers')
            console.log('Calling refetch...')
        } catch (error) {
            console.error('Error updating customer:', error)
        } finally {
            setIsLoading(false)
        }
    }

    console.log('isLoading:', isLoading)

    if (isLoading) return <div>Loading...</div>

    if (error) return <div>Error: {error.message}</div>

    return (
        <div className="bg-blue-950 min-h-screen flex items-center justify-center">
            <div className="bg-white shadow-xl rounded-lg p-8 border border-blue-300 max-w-md w-full">
                <h1 className="text-2xl font-bold mb-6 text-blue-800">
                    Update Customer
                </h1>
                <form onSubmit={handleSubmit} className="space-y-4">
                    <div>
                        <label className="block text-sm font-medium text-blue-700">
                            Name:
                        </label>
                        <input
                            type="text"
                            name="name"
                            value={formData.name}
                            onChange={handleChange}
                            className="mt-1 focus:ring-blue-400 focus:border-blue-400 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                        />
                    </div>

                    <div>
                        <label className="block text-sm font-medium text-blue-700">
                            Phone_number:
                        </label>
                        <input
                            type="number"
                            name="phone_number"
                            value={formData.phone_number}
                            onChange={handleChange}
                            className="mt-1 focus:ring-blue-400 focus:border-blue-400 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                        />
                    </div>

                    <div>
                        <label className="block text-sm font-medium text-blue-700">
                            Email:
                        </label>
                        <input
                            type="text"
                            name="email"
                            value={formData.email}
                            onChange={handleChange}
                            className="mt-1 focus:ring-blue-400 focus:border-blue-400 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                        />
                    </div>

                    <div>
                        <label className="block text-sm font-medium text-blue-700">
                            Vehicle_year:
                        </label>
                        <input
                            type="number"
                            name="vehicle_year"
                            value={formData.vehicle_year}
                            onChange={handleChange}
                            className="mt-1 focus:ring-blue-400 focus:border-blue-400 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                        />
                    </div>

                    <div>
                        <label className="block text-sm font-medium text-blue-700">
                            Vehicle_make:
                        </label>
                        <input
                            type="text"
                            name="vehicle_make"
                            value={formData.vehicle_make}
                            onChange={handleChange}
                            className="mt-1 focus:ring-blue-400 focus:border-blue-400 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                        />
                    </div>
                    <div>
                        <label className="block text-sm font-medium text-blue-700">
                            Vehicle_model:
                        </label>
                        <input
                            type="text"
                            name="vehicle_model"
                            value={formData.vehicle_model}
                            onChange={handleChange}
                            className="mt-1 focus:ring-blue-400 focus:border-blue-400 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                        />
                    </div>


                    <button
                        type="submit"
                        className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-500 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-400"
                    >
                        Update Customer
                    </button>
                </form>
            </div>
        </div>
    )
}

export default UpdateCustomer
