from queries.customer_queries import CustomerOut, ItemRepository, CustomerIn, Error
from typing import Union, Optional, List
from fastapi import Depends, Response, HTTPException, APIRouter
from authenticator import authenticator

router = APIRouter(tags=["customers"], prefix="/api/customers")

def get_item_repository():
    return ItemRepository()

@router.post("/customers", response_model=Union[CustomerOut, Error])
def add_customer(item: CustomerIn, response: Response, account_data: dict =
                 Depends(authenticator.get_current_account_data),
                 repo: ItemRepository = Depends(get_item_repository)):
    return repo.add_customer(item, account_id=account_data['id'])

@router.get("/customers/mine", response_model=Union[List[CustomerOut], Error])
def get_all_customers(account_data: dict =
                Depends(authenticator.get_current_account_data),
                repo: ItemRepository = Depends()):
    return repo.get_all_customers(account_id=account_data['id'])

@router.put("/customers/{item_id}", response_model=Union[CustomerOut, Error])
def update_customer(item_id: str, item: CustomerIn, account_data: dict =
                    Depends(authenticator.get_current_account_data),
                    repo: ItemRepository = Depends()) -> Union[Error, CustomerOut]:
    customer = repo.update_customer(item_id, account_data['id'], item)
    if customer is None:
        raise HTTPException(status_code=404, detail="customer not found")
    return customer

@router.delete("/customers/{item_id}", response_model=bool)
def delete_customer(item_id: str, account_data: dict =
                    Depends(authenticator.get_current_account_data),
                    repo: ItemRepository = Depends()) -> bool:
    return repo.delete_customer(item_id=item_id, account_id=account_data['id'])

@router.get("/customers/{item_id}", response_model=Optional[CustomerOut])
def get_customer(item_id: str, response: Response, account_data: dict =
                 Depends(authenticator.get_current_account_data),
                 repo: ItemRepository = Depends()) -> CustomerOut:
    item = repo.get_customer(item_id, account_id=account_data['id'])
    if item is None:
        response.status_code = 404
    return item
