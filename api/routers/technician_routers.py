from queries.technician_queries import TechnicianIn, ItemRepository, TechnicianOut, Error
from typing import Union, Optional, List
from fastapi import Depends, Response, HTTPException, APIRouter
from authenticator import authenticator

router = APIRouter(tags=["technicians"], prefix="/api/technicians")

def get_item_repository():
    return ItemRepository()

@router.post("/technicians", response_model=Union[TechnicianOut, Error])
def add_technician(item: TechnicianIn, response: Response, account_data: dict =
                 Depends(authenticator.get_current_account_data),
                 repo: ItemRepository = Depends(get_item_repository)):
    return repo.add_technician(item, technician_id=account_data['id'])

@router.get("/technicians/mine", response_model=Union[List[TechnicianOut], Error])
def get_all_technicians(account_data: dict =
                Depends(authenticator.get_current_account_data),
                repo: ItemRepository = Depends()):
    return repo.get_all_technicians(technician_id=account_data['id'])

@router.put("/technicians/{item_id}", response_model=Union[TechnicianOut, Error])
def update_technician(item_id: str, item: TechnicianIn, account_data: dict =
                    Depends(authenticator.get_current_account_data),
                    repo: ItemRepository = Depends()) -> Union[Error, TechnicianOut]:
    technician = repo.update_technician(item_id, account_data['id'], item)
    if technician is None:
        raise HTTPException(status_code=404, detail="technician not found")
    return technician

@router.delete("/technicians/{item_id}", response_model=bool)
def delete_technician(item_id: str, account_data: dict =
                    Depends(authenticator.get_current_account_data),
                    repo: ItemRepository = Depends()) -> bool:
    return repo.delete_technician(item_id=item_id, technician_id=account_data['id'])

@router.get("/technicians/{item_id}", response_model=Optional[TechnicianOut])
def get_technician(item_id: str, response: Response, account_data: dict =
                 Depends(authenticator.get_current_account_data),
                 repo: ItemRepository = Depends()) -> TechnicianOut:
    item = repo.get_technician(item_id, technician_id=account_data['id'])
    if item is None:
        response.status_code = 404
    return item
