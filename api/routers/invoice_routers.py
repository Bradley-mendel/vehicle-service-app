from queries.invoice_queries import InvoicesIn, ItemRepository, InvoicesOut, Error
from typing import Union, Optional, List
from fastapi import Depends, Response, HTTPException, APIRouter
from authenticator import authenticator

router = APIRouter(tags=["invoices"], prefix="/api/invoices")

def get_item_repository():
    return ItemRepository()

@router.post("/invoices", response_model=Union[InvoicesOut, Error])
def add_invoice(item: InvoicesIn, response: Response, account_data: dict =
                 Depends(authenticator.get_current_account_data),
                 repo: ItemRepository = Depends(get_item_repository)):
    return repo.add_invoice(item, account_id=account_data['id'])

@router.get("/invoices/mine", response_model=Union[List[InvoicesOut], Error])
def get_all_invoices(account_data: dict =
                Depends(authenticator.get_current_account_data),
                repo: ItemRepository = Depends()):
    return repo.get_all_invoices(account_id=account_data['id'])

@router.put("/invoices/{item_id}", response_model=Union[InvoicesOut, Error])
def update_invoice(item_id: str, item: InvoicesIn, account_data: dict =
                    Depends(authenticator.get_current_account_data),
                    repo: ItemRepository = Depends()) -> Union[Error, InvoicesOut]:
    invoice = repo.update_invoice(item_id, account_data['id'], item)
    if invoice is None:
        raise HTTPException(status_code=404, detail="invoice not found")
    return invoice

@router.delete("/invoices/{item_id}", response_model=bool)
def delete_invoice(item_id: str, account_data: dict =
                    Depends(authenticator.get_current_account_data),
                    repo: ItemRepository = Depends()) -> bool:
    return repo.delete_invoice(item_id=item_id, account_id=account_data['id'])

@router.get("/invoices/{item_id}", response_model=Optional[InvoicesOut])
def get_invoice(item_id: str, response: Response, account_data: dict =
                 Depends(authenticator.get_current_account_data),
                 repo: ItemRepository = Depends()) -> InvoicesOut:
    item = repo.get_invoice(item_id, account_id=account_data['id'])
    if item is None:
        response.status_code = 404
    return item
