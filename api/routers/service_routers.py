from queries.service_queries import ServiceOut, ItemRepository, ServicesIn, Error
from typing import Union, Optional, List
from fastapi import Depends, Response, HTTPException, APIRouter
from authenticator import authenticator


router = APIRouter(tags=["services"], prefix="/api/services")

def get_item_repository():
    return ItemRepository()

@router.post("/services", response_model=Union[ServiceOut, Error])
def add_service(item: ServicesIn, response: Response, account_data: dict =
                 Depends(authenticator.get_current_account_data),
                 repo: ItemRepository = Depends(get_item_repository)):
    return repo.add_service(item, technician_id=account_data['id'])

@router.get("/services/mine", response_model=Union[List[ServiceOut], Error])
def get_all_services(account_data: dict =
                Depends(authenticator.get_current_account_data),
                repo: ItemRepository = Depends()):
    return repo.get_all_services(technician_id=account_data['id'])

@router.put("/services/{item_id}", response_model=Union[ServiceOut, Error])
def update_service(item_id: str, item: ServicesIn, account_data: dict =
                    Depends(authenticator.get_current_account_data),
                    repo: ItemRepository = Depends()) -> Union[Error, ServiceOut]:
    service = repo.update_service(item_id, account_data['id'], item)
    if service is None:
        raise HTTPException(status_code=404, detail="service not found")
    return service

@router.delete("/services/{item_id}", response_model=bool)
def delete_service(item_id: str, account_data: dict =
                    Depends(authenticator.get_current_account_data),
                    repo: ItemRepository = Depends()) -> bool:
    return repo.delete_service(item_id=item_id, technician_id=account_data['id'])

@router.get("/services/{item_id}", response_model=Optional[ServiceOut])
def get_service(item_id: str, response: Response, account_data: dict =
                 Depends(authenticator.get_current_account_data),
                 repo: ItemRepository = Depends()) -> ServiceOut:
    item = repo.get_service(item_id, technician_id=account_data['id'])
    if item is None:
        response.status_code = 404
    return item
