from pydantic import BaseModel
from datetime import date

class Error(BaseModel):
    message: str


class ServicesIn(BaseModel):
    date: date
    customers_name: str
    technician: str
    vehicle_year: int
    vehicle_make: str
    vehicle_model: str
    service_suggested: str
    service_performed: str
    completed: bool


class ServiceOut(BaseModel):
    id: str
    technician_id: str
    date: date
    customers_name: str
    technician: str
    vehicle_year: int
    vehicle_make: str
    vehicle_model: str
    service_suggested: str
    service_performed: str
    completed: bool
