from pydantic import BaseModel
from datetime import date


class Error(BaseModel):
    message: str


class InvoicesIn(BaseModel):
    date: date
    name: str
    phone_number: int
    email: str
    vehicle_year: int
    vehicle_make: str
    vehicle_model: str
    cost: int
    paid: bool



class InvoicesOut(BaseModel):
    id: str
    account_id: str
    date: date
    name: str
    phone_number: int
    email: str
    vehicle_year: int
    vehicle_make: str
    vehicle_model: str
    cost: int
    paid: bool
