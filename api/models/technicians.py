from pydantic import BaseModel


class Error(BaseModel):
    message: str


class TechnicianIn(BaseModel):
    name: str
    number: int
    address: int


class TechnicianOut(BaseModel):
    id: str
    technician_id: str
    name: str
    number: int
    address: int
