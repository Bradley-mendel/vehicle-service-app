from pydantic import BaseModel


class Error(BaseModel):
    message: str


class CustomerIn(BaseModel):
    name: str
    phone_number: int
    email: str
    vehicle_year: int
    vehicle_make: str
    vehicle_model: str


class CustomerOut(BaseModel):
    id: str
    account_id: str
    name: str
    phone_number: int
    email: str
    vehicle_year: int
    vehicle_make: str
    vehicle_model: str
