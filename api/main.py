from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from routers import (customer_routers,
                     invoice_routers,
                     service_routers,
                     technician_routers,
                     accounts_routers)
import os
from authenticator import authenticator

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(authenticator.router, tags=['Login'])
app.include_router(customer_routers.router)
app.include_router(invoice_routers.router)
app.include_router(service_routers.router)
app.include_router(technician_routers.router)
app.include_router(accounts_routers.router)
