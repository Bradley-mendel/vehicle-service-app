from queries.client import MongoQueries
from bson import ObjectId
from typing import Optional, Union, List
from models.invoice import Error, InvoicesIn, InvoicesOut
from datetime import datetime

class DuplicateAccountError(ValueError):
    pass


class ItemRepository(MongoQueries):

    def get_invoice(self, item_id: str, account_id: str) -> Optional[InvoicesOut]:
        invoice_queries = MongoQueries(collection_name="invoices")
        record = invoice_queries.collection.find_one(
            {"_id": ObjectId(item_id), "account_id": account_id})
        if record:
            return self.record_to_item_out(record)
        else:
            return {"message": f"Could not find that {item_id}"}


    def delete_invoice(self, item_id: str, account_id: str) -> bool:
        invoice_queries = MongoQueries(collection_name="invoices")
        result = invoice_queries.collection.delete_one(
            {"_id": ObjectId(item_id), "account_id": account_id})
        return result.deleted_count > 0


    def get_all_invoices(self,
                            account_id: str) -> Union[Error, List[InvoicesOut]]:
        invoice_queries = MongoQueries(collection_name="invoices")
        try:
            records = invoice_queries.collection.find(
                {'account_id': account_id})
            return [self.record_to_item_out(record) for record in records]
        except Exception as e:
            return Error(message=str(e))


    def add_invoice(self, item: InvoicesIn,
                    account_id: str) -> Union[InvoicesOut, Error]:
        invoice_queries = MongoQueries(collection_name="invoices")
        try:
            item_dict = item.dict()
            item_dict['account_id'] = account_id
            if 'date' in item_dict:
                item_dict['date'] = datetime.combine(
                    item_dict['date'], datetime.min.time())
            result = invoice_queries.collection.insert_one(item_dict)
            item_dict["id"] = str(result.inserted_id)
            del item_dict["_id"]
            return InvoicesOut(**item_dict)
        except Exception as e:
            return Error(message=str(e))


    def item_in_to_out(self, id: int, account_id: str,
                    item: InvoicesIn) -> InvoicesOut:
        return InvoicesOut(id=id, account_id=account_id, **item.dict())


    def update_invoice(self, item_id: int, account_id: str,
                        item: InvoicesIn) -> Union[InvoicesOut, Error]:
        invoice_queries = MongoQueries(collection_name="invoices")
        item_dict = item.dict()
        item_dict['account_id'] = account_id
        if 'date' in item_dict:
            item_dict['date'] = datetime.combine(
                item_dict['date'], datetime.min.time())
        result = invoice_queries.collection.update_one(
            {"_id": ObjectId(item_id)},
            {"$set": item_dict}
        )
        if result.matched_count:
            return self.item_in_to_out(item_id, account_id, item)
        else:
            return {"message": f"Could not update {item.name}"}


    def record_to_item_out(self, record) -> InvoicesOut:
        if '_id' in record:
            record['id'] = str(record['_id'])
            del record['_id']
        if 'date' in record and isinstance(
                record['date'], datetime):
            record['date'] = record['date'].date()
        if 'name' in record:
            record['name'] = str(record['name'])
        if 'phone_number' in record:
            record['phone_number'] = str(record['phone_number'])
        if 'email' in record:
            record['email'] = str(record['email'])
        if 'vehicle_year' in record:
            record['vehicle_year'] = str(record['vehicle_year'])
        if 'vehicle_make' in record:
            record['vehicle_make'] = str(record['vehicle_make'])
        if 'vehicle_model' in record:
            record['vehicle_model'] = str(record['vehicle_model'])
        if 'cost' in record:
            record['cost'] = str(record['cost'])
        if 'paid' in record:
            record['paid'] = str(record['paid'])
        required_fields = ['id', 'date', 'name', 'phone_number', 'email', 'vehicle_year', 'vehicle_model', 'cost', 'paid']
        for field in required_fields:
            if field not in record:
                print(f'Missing field: {field}')
        return InvoicesOut(**record)
