from queries.client import MongoQueries
from bson import ObjectId
from typing import Optional, Union, List
from models.services import Error, ServicesIn, ServiceOut
from datetime import datetime

class DuplicateAccountError(ValueError):
    pass


class ItemRepository(MongoQueries):

    def get_service(self, item_id: str, technician_id: str) -> Optional[ServiceOut]:
        service_queries = MongoQueries(collection_name="services")
        record = service_queries.collection.find_one(
            {"_id": ObjectId(item_id), "technician_id": technician_id})
        if record:
            return self.record_to_item_out(record)
        else:
            return {"message": f"Could not find that {item_id}"}


    def delete_service(self, item_id: str, technician_id: str) -> bool:
        service_queries = MongoQueries(collection_name="services")
        result = service_queries.collection.delete_one(
            {"_id": ObjectId(item_id), "technician_id": technician_id})
        return result.deleted_count > 0


    def get_all_services(self,
                            technician_id: str) -> Union[Error, List[ServiceOut]]:
        service_queries = MongoQueries(collection_name="services")
        try:
            records = service_queries.collection.find(
                {'technician_id': technician_id})
            return [self.record_to_item_out(record) for record in records]
        except Exception as e:
            return Error(message=str(e))


    def add_service(self, item: ServicesIn,
                    technician_id: str) -> Union[ServiceOut, Error]:
        service_queries = MongoQueries(collection_name="services")
        try:
            item_dict = item.dict()
            item_dict['technician_id'] = technician_id
            if 'date' in item_dict:
                item_dict['date'] = datetime.combine(
                    item_dict['date'], datetime.min.time())
            result = service_queries.collection.insert_one(item_dict)
            item_dict["id"] = str(result.inserted_id)
            del item_dict["_id"]
            return ServiceOut(**item_dict)
        except Exception as e:
            return Error(message=str(e))


    def item_in_to_out(self, id: int, technician_id: str,
                    item: ServicesIn) -> ServiceOut:
        return ServiceOut(id=id, technician_id=technician_id, **item.dict())


    def update_service(self, item_id: int, technician_id: str,
                        item: ServicesIn) -> Union[ServiceOut, Error]:
        service_queries = MongoQueries(collection_name="services")
        item_dict = item.dict()
        item_dict['technician_id'] = technician_id
        if 'date' in item_dict:
            item_dict['date'] = datetime.combine(
                item_dict['date'], datetime.min.time())
        result = service_queries.collection.update_one(
            {"_id": ObjectId(item_id)},
            {"$set": item_dict}
        )
        if result.matched_count:
            return self.item_in_to_out(item_id, technician_id, item)
        else:
            return {"message": f"Could not update {item.name}"}


    def record_to_item_out(self, record) -> ServiceOut:
        if '_id' in record:
            record['id'] = str(record['_id'])
            del record['_id']
        if 'date' in record and isinstance(
                record['date'], datetime):
            record['date'] = record['date'].date()
        if 'customers_name' in record:
            record['customers_name'] = str(record['customers_name'])
        if 'technician' in record:
            record['technician'] = str(record['technician'])
        if 'vehicle_year' in record:
            record['vehicle_year'] = str(record['vehicle_year'])
        if 'vehicle_make' in record:
            record['vehicle_make'] = str(record['vehicle_make'])
        if 'vehicle_model' in record:
            record['vehicle_model'] = str(record['vehicle_model'])
        if 'service_suggested' in record:
            record['service_suggested'] = str(record['service_suggested'])
        if 'service_performed' in record:
            record['service_performed'] = str(record['service_performed'])
        if 'completed' in record:
            record['completed'] = str(record['completed'])
        required_fields = ['id', 'date', 'customers_name', 'technician', 'vehicle_year', 'vehicle_model', 'service_suggested', 'service_performed', 'completed']
        for field in required_fields:
            if field not in record:
                print(f'Missing field: {field}')
        return ServiceOut(**record)
