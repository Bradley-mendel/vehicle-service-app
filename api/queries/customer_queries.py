from queries.client import MongoQueries
from bson import ObjectId
from typing import Optional, Union, List
from models.customers import Error, CustomerIn, CustomerOut


class DuplicateAccountError(ValueError):
    pass


class ItemRepository(MongoQueries):


    def get_customer(self, item_id: str, account_id: str) -> Optional[CustomerOut]:
        customer_queries = MongoQueries(collection_name="customers")
        record = customer_queries.collection.find_one(
            {"_id": ObjectId(item_id), "account_id": account_id})
        if record:
            return self.record_to_item_out(record)
        else:
            return {"message": f"Could not find that {item_id}"}

    def delete_customer(self, item_id: str, account_id: str) -> bool:
        customer_queries = MongoQueries(collection_name="customers")
        result = customer_queries.collection.delete_one(
            {"_id": ObjectId(item_id), "account_id": account_id})
        return result.deleted_count > 0


    def get_all_customers(self,
                            account_id: str) -> Union[Error, List[CustomerOut]]:
        customer_queries = MongoQueries(collection_name="customers")
        try:
            records = customer_queries.collection.find(
                {'account_id': account_id})
            return [self.record_to_item_out(record) for record in records]
        except Exception as e:
            return Error(message=str(e))


    def add_customer(self, item: CustomerIn,
                    account_id: str) -> Union[CustomerOut, Error]:
        customer_queries = MongoQueries(collection_name="customers")
        try:
            item_dict = item.dict()
            item_dict['account_id'] = account_id
            result = customer_queries.collection.insert_one(item_dict)
            item_dict["id"] = str(result.inserted_id)
            del item_dict["_id"]
            return CustomerOut(**item_dict)
        except Exception as e:
            return Error(detail=str(e))


    def item_in_to_out(self, id: int, account_id: str,
                    item: CustomerIn) -> CustomerOut:
        return CustomerOut(id=id, account_id=account_id, **item.dict())


    def update_customer(self, item_id: int, account_id: str,
                        item: CustomerIn) -> Union[CustomerOut, Error]:
        customer_queries = MongoQueries(collection_name="customers")
        item_dict = item.dict()
        item_dict['account_id'] = account_id
        result = customer_queries.collection.update_one(
            {"_id": ObjectId(item_id)},
            {"$set": item_dict}
        )
        if result.matched_count:
            return self.item_in_to_out(item_id, account_id, item)
        else:
            return {"message": f"Could not update {item.name}"}


    def record_to_item_out(self, record) -> CustomerOut:
        if '_id' in record:
                record['id'] = str(record['_id'])
                del record['_id']
        if 'name' in record:
            record['name'] = str(record['name'])
        if 'phone_number' in record:
            record['phone_number'] = str(record['phone_number'])
        if 'email' in record:
            record['email'] = str(record['email'])
        if 'vehicle_year' in record:
            record['vehicle_year'] = str(record['vehicle_year'])
        if 'vehicle_make' in record:
            record['vehicle_make'] = str(record['vehicle_make'])
        if 'vehicle_model' in record:
            record['vehicle_model'] = str(record['vehicle_model'])
        required_fields = ['id', 'name', 'phone_number', 'email', 'vehicle_year', 'vehicle_model']
        for field in required_fields:
            if field not in record:
                print(f'Missing field: {field}')
        return CustomerOut(**record)
