from queries.client import MongoQueries
from bson import ObjectId
from typing import Optional, Union, List
from models.technicians import Error, TechnicianIn, TechnicianOut


class DuplicateAccountError(ValueError):
    pass


class ItemRepository(MongoQueries):


    def get_technician(self, item_id: str, technician_id: str) -> Optional[TechnicianOut]:
        technician_queries = MongoQueries(collection_name="technicians")
        record = technician_queries.collection.find_one(
            {"_id": ObjectId(item_id), "technician_id": technician_id})
        if record:
            return self.record_to_item_out(record)
        else:
            return {"message": f"Could not find that {item_id}"}


    def delete_technician(self, item_id: str, technician_id: str) -> bool:
        technician_queries = MongoQueries(collection_name="technicians")
        result = technician_queries.collection.delete_one(
            {"_id": ObjectId(item_id), "technician_id": technician_id})
        return result.deleted_count > 0


    def get_all_technicians(self,
                            technician_id: str) -> Union[Error, List[TechnicianOut]]:
        technician_queries = MongoQueries(collection_name="technicians")
        try:
            records = technician_queries.collection.find(
                {'technician_id': technician_id})
            return [self.record_to_item_out(record) for record in records]
        except Exception as e:
            return Error(message=str(e))


    def add_technician(self, item: TechnicianIn,
                    technician_id: str) -> Union[TechnicianOut, Error]:
        technician_queries = MongoQueries(collection_name="technicians")
        try:
            item_dict = item.dict()
            item_dict['technician_id'] = technician_id
            result = technician_queries.collection.insert_one(item_dict)
            item_dict["id"] = str(result.inserted_id)
            del item_dict["_id"]
            return TechnicianOut(**item_dict)
        except Exception as e:
            return Error(detail=str(e))

    def item_in_to_out(self, id: int, technician_id: str,
                    item: TechnicianIn) -> TechnicianOut:
        return TechnicianOut(id=id, technician_id=technician_id, **item.dict())

    def update_technician(self, item_id: int, technician_id: str,
                        item: TechnicianIn) -> Union[TechnicianOut, Error]:
        technician_queries = MongoQueries(collection_name="technicians")
        item_dict = item.dict()
        item_dict['technician_id'] = technician_id
        result = technician_queries.collection.update_one(
            {"_id": ObjectId(item_id)},
            {"$set": item_dict}
        )
        if result.matched_count:
            return self.item_in_to_out(item_id, technician_id, item)
        else:
            return {"message": f"Could not update {item.name}"}


    def record_to_item_out(self, record) -> TechnicianOut:
        if '_id' in record:
            record['id'] = str(record['_id'])
            del record['_id']
        if 'name' in record:
            record['name'] = str(record['name'])
        if 'number' in record:
            record['number'] = str(record['number'])
        if 'address' in record:
            record['address'] = str(record['address'])
        required_fields = ['id', 'name', 'number', 'address']
        for field in required_fields:
            if field not in record:
                print(f'Missing field: {field}')
        return TechnicianOut(**record)
